<?php

/**
 * @file
 * Page callbacks and utility functions for IP Anonymize module.
 */

/**
 * Anonymize IP addresses which have exceeded the retention period.
 */
function ip_anon_scrub(): void {
  $config = \Drupal::config('ip_anon.settings');
  foreach (ip_anon_tables() as $table => $columns) {
    $period = $config->get("period_$table");
    if ($period < 0) {
      continue;
    }
    $query = \Drupal::database()->update($table)
      ->fields([$columns['hostname'] => '0'])
      ->condition($columns['timestamp'], strval(\Drupal::time()->getRequestTime() - $period), '<=');
    // Workaround for Drupal core setting PDO::MYSQL_ATTR_FOUND_ROWS, which
    // means we don't actually know how many rows were affected by an update
    // query, unless we use conditions.
    if (!empty($columns['callback'])) {
      $query->condition($columns['hostname'], '0', '<>');
    }
    $rows = $query->execute();
    if (!empty($columns['callback']) && $rows) {
      $columns['callback']();
    }
  }
}

/**
 * Default array of hostname and timestamp columns.
 *
 * @return string[]
 *   Default columns.
 */
function ip_anon_columns(): array {
  $columns = ['hostname', 'timestamp'];
  return array_combine($columns, $columns);
}

/**
 * Array of tables and columns which store hostnames and timestamps.
 *
 * Modules may add tables by implementing hook_ip_anon_alter().
 *
 * @return array<string, array{hostname: string, timestamp: string, callback?: callable}>
 *   List of tables to be scrubbed.
 */
function ip_anon_tables(): array {
  $tables = ['sessions' => ip_anon_columns()];
  \Drupal::moduleHandler()->alter('ip_anon', $tables);
  return $tables;
}

/**
 * Implements hook_ip_anon_alter() for comment module.
 *
 * @phpstan-ignore missingType.iterableValue
 */
function comment_ip_anon_alter(array &$tables): void {
  $tables['comment_field_data'] = [
    'hostname' => 'hostname',
    'timestamp' => 'changed',
  ];
}

/**
 * Implements hook_ip_anon_alter() for dblog module.
 *
 * @phpstan-ignore missingType.iterableValue
 */
function dblog_ip_anon_alter(array &$tables): void {
  $tables['watchdog'] = ip_anon_columns();
}

/**
 * Implements hook_ip_anon_alter() for Commerce Order module.
 *
 * @phpstan-ignore missingType.iterableValue
 */
function commerce_order_ip_anon_alter(array &$tables): void {
  $tables['commerce_order'] = [
    'hostname' => 'ip_address',
    'timestamp' => 'changed',
    'callback' => function () {
      \Drupal::entityTypeManager()->getStorage('commerce_order')->resetCache();
    },
  ];
}

/**
 * Implements hook_ip_anon_alter() for Login History module.
 *
 * @phpstan-ignore missingType.iterableValue
 */
function login_history_ip_anon_alter(array &$tables): void {
  $tables['login_history'] = [
    'hostname' => 'hostname',
    'timestamp' => 'login',
  ];
}

/**
 * Implements hook_ip_anon_alter() for simple_access_log module.
 *
 * @phpstan-ignore missingType.iterableValue
 */
function simple_access_log_ip_anon_alter(array &$tables): void {
  $tables['simple_access_log'] = [
    'hostname' => 'remote_host',
    'timestamp' => 'timestamp',
  ];
}

/**
 * Implements hook_ip_anon_alter() for Tether Stats module.
 *
 * @phpstan-ignore missingType.iterableValue
 */
function tether_stats_ip_anon_alter(array &$tables): void {
  $tables['tether_stats_activity_log'] = [
    'hostname' => 'ip_address',
    'timestamp' => 'created',
  ];
}

/**
 * Implements hook_ip_anon_alter() for Ubercart Order module.
 *
 * @phpstan-ignore missingType.iterableValue
 */
function uc_order_ip_anon_alter(array &$tables): void {
  $tables['uc_orders'] = [
    'hostname' => 'host',
    'timestamp' => 'changed',
  ];
}

/**
 * Implements hook_ip_anon_alter() for User IP Log module.
 *
 * @phpstan-ignore missingType.iterableValue
 */
function uiplog_ip_anon_alter(array &$tables): void {
  $tables['uiplog'] = [
    'hostname' => 'ip',
    'timestamp' => 'timestamp',
  ];
}

/**
 * Implements hook_ip_anon_alter() for Visitors module.
 *
 * @phpstan-ignore missingType.iterableValue
 */
function visitors_ip_anon_alter(array &$tables): void {
  $tables['visitors'] = [
    'hostname' => 'visitors_ip',
    'timestamp' => 'visitors_date_time',
  ];
}

/**
 * Implements hook_ip_anon_alter() for Voting API module.
 *
 * @phpstan-ignore missingType.iterableValue
 */
function votingapi_ip_anon_alter(array &$tables): void {
  $tables['votingapi_vote'] = [
    'hostname' => 'vote_source',
    'timestamp' => 'timestamp',
  ];
}

/**
 * Implements hook_ip_anon_alter() for webform module.
 *
 * @phpstan-ignore missingType.iterableValue
 */
function webform_ip_anon_alter(array &$tables): void {
  $tables['webform_submission'] = [
    'hostname' => 'remote_addr',
    'timestamp' => 'changed',
    'callback' => function () {
      // @phpstan-ignore-next-line Webform is not yet a dev dependency.
      \Drupal::entityTypeManager()->getStorage('webform_submission')->resetCache();
    },
  ];
}

/**
 * Implements hook_ip_anon_alter() for yamlform module.
 *
 * @phpstan-ignore missingType.iterableValue
 */
function yamlform_ip_anon_alter(array &$tables): void {
  $tables['yamlform_submission'] = [
    'hostname' => 'remote_addr',
    'timestamp' => 'changed',
  ];
}
