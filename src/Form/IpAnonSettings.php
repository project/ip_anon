<?php

namespace Drupal\ip_anon\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for ip_anon module.
 */
class IpAnonSettings extends ConfigFormBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs an IpAnonSettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  final public function __construct(ConfigFactoryInterface $config_factory, Connection $connection, DateFormatterInterface $date_formatter, ModuleHandlerInterface $module_handler, TypedConfigManagerInterface $typed_config_manager) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->connection = $connection;
    $this->dateFormatter = $date_formatter;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('database'),
      $container->get('date.formatter'),
      $container->get('module_handler'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ip_anon_settings';
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   Settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('ip_anon.settings');
    $config->set('policy', $form_state->getValue('policy'));
    foreach (Element::children($form['period']) as $variable) {
      $config->set($variable, $form_state->getValue($variable));
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @return string[]
   *   Editable config names.
   */
  protected function getEditableConfigNames() {
    return ['ip_anon.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   Settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed[]
   *   Settings form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ip_anon.settings');
    $form['policy'] = [
      '#type' => 'radios',
      '#title' => $this->t('Retention policy'),
      '#options' => [
        $this->t('Preserve IP addresses'),
        $this->t('Anonymize IP addresses'),
      ],
      '#description' => $this->t('This setting may be used to temporarily disable IP anonymization.'),
      '#default_value' => $config->get('policy'),
    ];
    $form['period'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Retention period'),
      '#description' => $this->t('IP addresses older than the retention period will be anonymized.'),
    ];
    $intervals = [
      0,
      30,
      60,
      120,
      180,
      300,
      600,
      900,
      1800,
      2700,
      3600,
      5400,
      7200,
      10800,
      21600,
      32400,
      43200,
      64800,
      86400,
      172800,
      259200,
      345600,
      518400,
      604800,
      1209600,
      2419200,
      4838400,
      9676800,
      31536000,
    ];
    $options = array_combine($intervals, array_map([
      $this->dateFormatter,
      'formatInterval',
    ], $intervals));
    $options[-1] = $this->t('Forever');
    $this->moduleHandler->loadInclude('ip_anon', 'inc');
    foreach (ip_anon_tables() as $table => $columns) {
      $form['period']["period_$table"] = [
        '#type' => 'select',
        '#title' => $this->t('@table table', ['@table' => $table]),
        '#options' => $options,
        '#default_value' => $config->get("period_$table"),
        '#description' => new FormattableMarkup('@description', ['@description' => $this->getTableDescription($table)]),
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * Returns table description.
   */
  public function getTableDescription(string $table): string {
    $modules = ['sessions' => 'system', 'watchdog' => 'dblog'];
    if (isset($modules[$table])) {
      $this->moduleHandler->loadInclude($modules[$table], 'install');
      $schema = $this->moduleHandler->invoke($modules[$table], 'schema');
      if (is_array($schema) && isset($schema[$table]['description'])) {
        return $schema[$table]['description'];
      }
    }
    if (method_exists($this->connection->schema(), 'getComment')) {
      return $this->connection->schema()->getComment($table);
    }
    // Sqlite doesn't have a getComment() method.
    return '';
  }

}
