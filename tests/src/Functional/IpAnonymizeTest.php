<?php

namespace Drupal\Tests\ip_anon\Functional;

use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\Traits\Core\CronRunTrait;

/**
 * Tests basic IP Anonymize functionality.
 *
 * @group IP Anonymize
 */
class IpAnonymizeTest extends BrowserTestBase {

  use CronRunTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['ip_anon', 'dblog'];

  /**
   * Basic tests for IP Anonymize module.
   */
  public function testIpAnonymize(): void {
    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->assertInstanceOf(AccountInterface::class, $admin_user);
    $this->drupalLogin($admin_user);

    $this->assertNotEmpty($this->getIp());

    $this->drupalGet('admin/config/people/ip_anon');
    $config['policy'] = '1';
    $config['period_watchdog'] = 0;
    $this->submitForm($config, 'Save configuration');

    $this->cronRun();

    $this->assertSame('0', $this->getIp());
  }

  /**
   * Get IP address from watchdog table.
   *
   * @return string|false
   *   The watchdog.hostname field.
   */
  protected function getIp() {
    $statement = $this->container->get('database')->select('watchdog', 'w')
      ->fields('w', ['hostname'])
      ->orderBy('wid')
      ->range(0, 1)
      ->execute();
    $this->assertInstanceOf(StatementInterface::class, $statement);
    $ip = $statement->fetchField();
    $this->assertTrue(is_string($ip) || FALSE === $ip);
    return $ip;
  }

}
